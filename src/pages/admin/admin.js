import Vue from 'vue'
import App from './admin.vue'
import layer from 'vue-layer';
import store from '@/store'
import './permission' // 权限
import router from './router'
import Api from '@/api';

// 导入bootstrap
import 'bootstrap337/dist/css/bootstrap.min.css'
import '@/assets/style/admin/animate.css'
import '@/assets/style/admin/style.css'
import '@/assets/style/bootstrap-select.min.css'


Vue.config.productionTip = false
Vue.prototype.api = Api
Vue.prototype.layer = layer(Vue);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
