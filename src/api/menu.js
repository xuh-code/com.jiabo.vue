import request from '@/util/axios'

export function GetMenu () {
  return request({
    url: '/admin/menu',
    method: 'get'
  })
}