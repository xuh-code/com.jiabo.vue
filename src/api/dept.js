import request from '@/util/axios'

export function fetchTree (query) {
  return request({
    url: '/admin/dept/tree',
    method: 'get',
    params: query
  })
}

export function addDept (obj) {
  return request({
    url: '/admin/dept/',
    method: 'post',
    data: obj
  })
}

export function getDept (id) {
  return request({
    url: '/admin/dept/' + id,
    method: 'get'
  })
}

export function delDept (id) {
  return request({
    url: '/admin/dept/' + id,
    method: 'delete'
  })
}

export function putDept (obj) {
  return request({
    url: '/admin/dept/',
    method: 'put',
    data: obj
  })
}