import * as Login from './login';
import * as Menu from './menu';
import * as User from './user';
import * as Dept from './dept';
import * as Role from './role';
import * as Dict from './dict';

export default {
  ...Login,
  ...Menu,
  ...User,
  ...Dept,
  ...Role,
  ...Dict,
};
