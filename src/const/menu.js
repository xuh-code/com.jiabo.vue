export default {
  // 配置菜单的属性
  props: {
    label: 'label',
    path: 'path',
    icon: 'icon',
    children: 'children'
  }
}
